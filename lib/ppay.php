<?php

namespace PeachPayments;


/**
 *  Engine object for payment links
 */
class PaymentLinksClient
{
    private Auth $auth;

    public Payment $payment;

    /**
     * @throws AuthenticationException
     */
    public function __construct(AuthDetails $authDetails)
    {
        $this->auth = new Auth($authDetails);

        try {
            $this->payment = new Payment($this->auth->getAuthToken());
            $this->setLive();
        } catch (AuthenticationException $e) {
            echo $e->getMessage(); //Can either throw or echo it, could probably have a setting to change this response behaviour
            //            throw $e;
        }
    }

    /**
     * To change to test mode
     */
    public function setTest()
    {
        $this->payment->updateApiBaseUrl("https://v2-sandbox.ppay.io");
    }

    /**
     * To change to live mode
     */
    public function setLive()
    {
        $this->payment->updateApiBaseUrl("https://links.peachpayments.com");
    }
}
