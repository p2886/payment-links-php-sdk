<?php

namespace PeachPayments;

/**
 *  Handles automatic authentication with peach payments oauth
 */
class Auth
{
    private AuthDetails $details;
    public string $authToken = '';

    public function __construct(AuthDetails $authDetails)
    {
        $this->details = $authDetails;
    }

    /**
     * Makes a request and handles the authentication
     * @throws AuthenticationException
     */
    public function getAuthToken(): string
    {
        $response = HttpClient::post(
            $this->details->authUrl,
            json_encode([
                "clientId" => $this->details->client_id,
                "clientSecret" => $this->details->client_secret,
                "merchantId" => $this->details->merchant_id,
            ]),
            [
                'Content-Type: application/json',
            ]
        );

        if (property_exists($response, 'error')) {
            throw new AuthenticationException(json_encode($response));
        }

        $this->authToken = $response->access_token;

        return $this->authToken;
    }
}
