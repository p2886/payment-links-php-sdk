<?php

namespace PeachPayments;

use DateTime;

/**
 *  Payment object to handle CRUD of payment links
 */
class Payment
{
    private string $authToken;
    private string $apiBaseUrl = "";

    public function __construct(string $authToken)
    {
        $this->authToken = $authToken;
    }

    /**
     * Create a new payment link
     * @throws \JsonException
     */
    public function create(string $entity_id, array $paymentDetails)
    {
        return HttpClient::post(
            strtr($this->apiBaseUrl . '/api/channels/:entity_id/payments', [":entity_id" => $entity_id]),
            json_encode($paymentDetails),
            [
                'Content-Type: application/json',
                'Authorization: Bearer ' . $this->authToken
            ]
        );
    }

    /**
     * To cancel a current valid payment link
     * @param string $paymentId
     * @return mixed|void
     * @throws \JsonException
     */
    public function cancel(string $paymentId)
    {
        return HttpClient::request(
            strtr($this->apiBaseUrl . '/api/payments/:paymentId', [":paymentId" => $paymentId]),
            'DELETE',
            [],
            [
                'Authorization: Bearer ' . $this->authToken
            ]
        );
    }

    /**
     * Get the status of a valid payment link
     * @param string $paymentId
     * @return mixed|void
     */
    public function status(string $paymentId)
    {
        return HttpClient::get(
            strtr($this->apiBaseUrl . '/api/payments/:paymentId', [":paymentId" => $paymentId]),
            [
                'Authorization: Bearer ' . $this->authToken
            ]
        );
    }

    /**
     * Get all transactions on payment links
     * @param int $page The page to retrieve
     * @param int $perPage The number of items per page
     * @param array $statuses Filter the items based on their status. e.g. ['initiated', 'processing']
     * @param DateTime $startDate The start date of the filter
     * @param DateTime $endDate The end date of the filter
     * @return mixed|void
     */
    public function all(int $page = 0, int $perPage = 30, array $statuses = [], DateTime $startDate = null, DateTime $endDate = null)
    {
        $statusFilter = '';

        for ($i = 0; $i < count($statuses); $i++) {
            $statusFilter .= '&filters[status][' . $i . ']=' . $statuses[$i];
        }

        $startDateFilter = '';
        if ($startDate) {
            $startDateFilter = '&filters[startDate]=' . explode("+", $startDate->format("c"))[0] . 'Z';
        }
        $endDateFilter = '';
        if ($endDate) {
            $endDateFilter = '&filters[endDate]=' . explode("+", $endDate->format("c"))[0] . 'Z';
        }

        return HttpClient::get(
            $this->apiBaseUrl . '/api/payments?perPage=' . $perPage . '&offset=' . ($page * $perPage) . $statusFilter . $startDateFilter . $endDateFilter,
            [
                'Authorization: Bearer ' . $this->authToken
            ]
        );
    }

    /**
     * Depending on test mode or live test
     * @param string $param
     * @return void
     */
    public function updateApiBaseUrl(string $param): void
    {
        $this->apiBaseUrl = $param;
    }
}
