<?php

namespace PeachPayments;

class AuthDetails
{
    public string $client_id = '';
    public string $client_secret = '';
    public string $merchant_id = '';

    public string $authUrl = 'https://dashboard.peachpayments.com/api/oauth/token';

    public function __construct(string $client_id, string $client_secret, string $merchant_id)
    {
        $this->client_id = $client_id;
        $this->client_secret = $client_secret;
        $this->merchant_id = $merchant_id;
    }
}
