<?php

namespace PeachPayments;

/**
 *  Currently only CURL client
 */
class HttpClient
{
    /**
     * Basic curl request
     * @throws \JsonException
     */
    public static function request(string $url, string $method, $postFields = [], ?array $headers = [])
    {
        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $method,
            CURLOPT_POSTFIELDS => $postFields,
            CURLOPT_HTTPHEADER => $headers,
        ]);

        //TODO:: remove vulnerability
        // curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        // curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);

        $response = json_decode(curl_exec($curl), false);
        if (!$response) {
            print_r(curl_error($curl));
            die();
        }

        curl_close($curl);

        return $response;
    }

    /**
     * request with GET method
     * @param string $url
     * @param $postFields
     * @param array|null $headers
     * @return mixed|void
     * @throws \JsonException
     */
    public static function get(string $url, ?array $headers = [])
    {
        return Self::request($url, 'GET', [], $headers);
    }

    /**
     * request with POST method
     * @param string $url
     * @param $postFields
     * @param array|null $headers
     * @return mixed|void
     * @throws \JsonException
     */
    public static function post(string $url, $postFields = [], ?array $headers = [])
    {
        return Self::request($url, 'POST', $postFields, $headers);
    }
}