# Payment Links PHP SDK

## Requirements

PHP 7.4.0 and later.

## Composer

You can install the sdk via [Composer](http://getcomposer.org/). Run the following command:

```bash
composer require peach/peach-payment-php
```

To use the sdk, use Composer's [autoload](https://getcomposer.org/doc/01-basic-usage.md#autoloading):

```php
require_once('vendor/autoload.php');
```

## Manual Installation

If you do not wish to use Composer, you can download
the [latest release](https://gitlab.com/p2886/payment-links-php-sdk).
Then, to use the sdk, include the `init.php` file.

```php
require_once('/path/to/peach-payment-php/init.php');
```

## Dependencies

The sdk require the following extensions in order to work properly:

- [`curl`](https://secure.php.net/manual/en/book.curl.php)
- [`json`](https://secure.php.net/manual/en/book.json.php)
- [`mbstring`](https://secure.php.net/manual/en/book.mbstring.php)

If you use Composer, these dependencies should be handled automatically. If you install manually, you'll want to make
sure that these extensions are available.

## Getting Started

Simple usage looks like:

```php
const CLIENT_ID = "";
const CLIENT_SECRET = "";
const MERCHANT_ID = "";
const ENTITY_ID = "";

$ppay = new \PeachPayments\PaymentLinksClient(new \PeachPayments\AuthDetails(CLIENT_ID, CLIENT_SECRET, MERCHANT_ID));
$response = $ppay->payment->create(ENTITY_ID, [
    ...PaymentDetails
]);

print_r($response);
```

Other examples can be found in the Examples Directory

## Development

### Testing