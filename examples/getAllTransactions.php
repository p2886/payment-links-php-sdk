<?php

use PeachPayments\PaymentLinksClient;
use PeachPayments\AuthDetails;

require(__DIR__ . "/../init.php");
require "config.php";

$auth = new AuthDetails(CLIENT_ID, CLIENT_SECRET, MERCHANT_ID);

$pp = new PaymentLinksClient($auth);
$pp->setTest();
$response = $pp->payment->all(0, 30, [], new DateTime("2022-10-12"));

print_r($response);
