<?php

use PeachPayments\PaymentLinksClient;
use PeachPayments\AuthDetails;

require(__DIR__ . "/../init.php");
require "config.php";

$auth = new AuthDetails(CLIENT_ID, CLIENT_SECRET, MERCHANT_ID);

$pp = new PaymentLinksClient($auth);
$pp->setTest();

$response = $pp->payment->status('79f69a10-d3d1-44eb-a45e-113364cd944d');

print_r($response);
