<?php

use PeachPayments\PaymentLinksClient;
use PeachPayments\AuthDetails;

require(__DIR__ . "/../init.php");
require "config.php";

$auth = new AuthDetails(CLIENT_ID, CLIENT_SECRET, MERCHANT_ID);

$pp = new PaymentLinksClient($auth);
$pp->setTest();

$response = $pp->payment->create(ENTITY_ID, [
    'payment' => [
        'merchantInvoiceId' => 'INV-0001',
        'amount' => 100.50,
        'currency' => 'ZAR',
        'files' => [],
        'notes' => 'Any additional notes go here.',
    ],
    'customer' => [
        'givenName' => 'Grace',
        'surname' => 'Gorczany',
        'email' => '',
        'mobile' => '',
        'whatsApp' => '',
        'billing' => [
            'street1' => '35 Brickfield Road',
            'city' => 'Cape Town',
            'state' => 'Western Cape',
            'postalCode' => '6112',
            'country' => 'ZA',
        ],
    ],
    'options' => [
        'sendEmail' => false,
        'sendSms' => false,
        'sendWhatsapp' => false,
        'emailCc' => '',
        'emailBcc' => '',
        'expiryTime' => 5,
        'notificationUrl' => '',
    ],
    'checkout' => [
        'defaultPaymentMethod' => 'CARD',
        'tokeniseCard' => false,
    ],
]);

print_r($response);